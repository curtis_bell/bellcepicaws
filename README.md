
Setting up your linux client/pc/ bastion host.

sudo yum install ansible docker epel-release.
Sudo systemctl start docker && sudo sytemctl enable docker
Sudo systemctl stop firewalld
edit /etc/ansible.cfg, change the roles
I wrote two sed’s to do this.
sudo sed -i 's/#host_key_checking = False/host_key_checking = False/g' ansible.cfg
The role path is important for later. If ansible cannot find the roles it will moan about it. Mine is set to where I clone my gitrepo.
sudo sed -i 's!#roles_path    = /etc/ansible/roles!roles_path    = /home/bellc/bellcepicaws/roles!g' ansible.cfg

Installing aws cli
sudo yum install awscli -y
this will install python as well.


Setting up aws
•	Create a login with aws.
•	Find the IAM service.
•	Add user
•	Tick Programmatic Access, AWS Management Console access.
•	Give yourself a password
•	Click next on the stages.
•	Attack the AdministratorAccess to your user as well, with “add permissions”
•	You should now have a user with an access key ID.



aws ec2 create-key-pair --key-name bellcawsepic --query 'KeyMaterial' --output text > bellcawsepic.pem

Configure your aws region
•	Run aws configure from your linux command line.
•	Enter the access key ID we created from the IAM console.
•	Provide the secret access key, also in the same page within IAM.
•	I used region eu-west-2
•	Default output format I used text.
Create security group
You will have to create your own VPC, I already have one created, you will use this id to put in  VPC-ID.
Note: when your VPC is created you will get allocated some subnets to use at the next phase.
Take note of your subnet ID with your  and your security group id
aws ec2 create-security-group --group-name bellcepic-sg --description "Default bellc security group" --vpc-id vpc-0ac96263

note: Make sure you open up port 22 here, 8080,

Create an EC2 Instance.
We will create a master and a node.
aws ec2 run-instances --image-id ami-00846a67 --count 2 --instance-type t2.micro --key-name bellcawsepic --security-group-ids sg-0af42b99ee56e7262 --subnet-id subnet-7fd10d04


Connect to the instance as a test.
From my linux machine I changed permissions and performed the test

Chmod 400 mykey.pem

ssh -i "mykey.pem" ec2-user@ec2-35-177-120-132.eu-west-2.compute.amazonaws.com


Testing Ansible

You should now have ansible configured above, and cloned the repo I made.
I made a simple ansible playbook to just install vim just so I can test connectivity and my key works.

ansible-playbook -i hosts ansibletest.yaml --key-file=/home/bellc/bellcawsepic.pem

Running the kubernetes master role.

Run the anible playbook to make the master
ansible-playbook -i ~/bellcepicaws/hosts ~/bellcepicaws/playbooks/wrappers/kubernetes/master/kubernetesmaster.yaml

SSH into the master, run kubectl cluster-info, you should have localhost 8080 running.

Running the kuberentes node

Before you run the script for the node, change the variable for your master in the playbooks/wrappers/nodes/kubernetesnode.yaml

Build your docker image from the script
Docker/backend & frontend.
Dockerfile – contains all the contents for the build.
Dockerbuild- runs the build with tags on the previous file.
Dockerrun – simple bash script for testing so you don’t need to write out the command each time, can use this to run locally on your machine.

Export and scp the image to the kubernetes cluster

sudo docker save -o ./frontend.tar frontend:latest
sudo scp -i ~/.ssh/bellcawsepic.pem frontend.tar centos@18.130.108.88:/home/centos

do the same for the node


SSh onto the master, and node, docker wasn’t running for me, so make sure you start that service.

Sudo systemctl start docker && systemctl enable

Load the docker image into local repo

sudo docker load -i frontend.tar

SCP deployment yaml file over to the master

Conclusion.
Everything in AWS working as I hoped. Didn’t want to use AWS AKS because thought it would be cheating and too easy and wonder to show off some automation with ansible and some scripting.
The ansible role I provided does get the kubernetes cluster up and running as intended.
I didn’t get to finish test. I had a lot of problems getting my deployment to work in my cluster, I think im using an older version of kubernetes that doesn’t support it.
I tried to use the old way of using replication controller(now called replication sets) and services but was having some issues with nodeport. I can remember every time I have setup a kubernetes cluster, its always been a dog to get the last piece of the networking working nicely on the cloud.
I didn’t have time to build a docker repo, having a docker repo means you wouldn’t have to scp all the files over but was a quick work around.
I attached the deployment file I would of used. When the deployment is running, all you would do would be to run kubectl deployment=app-reployment set image = frontend or something similar.
Similarly, if the docker repo was in place, kubernetes would have access to just pull from the repo, as long as you setup the password in the deployment that you set up in


What id do different next time:-
I would have used terraform instead of ansible for setting up various components, I know you can use dependencies. Reason I didn’t use this in the test is I didn’t want to spend hours learning it and loosing motivation while trying to complete the test.
